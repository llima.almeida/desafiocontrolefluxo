package br.com.desafiocontrolefluxo.controle;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import br.com.desafiocontrolefluxo.exception.ParametrosInvalidosException;

public class Contador {
	 int indice = 0;  
     Scanner entrada = new Scanner(System.in);
     int numero1 = entrada.nextInt();
     int numero2 = entrada.nextInt();
     int inteiros[] = new int[2];
     inteiros[0] = numero1;
     inteiros[1] = numero2;
     for(int inteiro : inteiros){
         indice++;
         System.out.println("Imprimindo o número " + indice + ": " + inteiro );
     }
     if(numero1 >  numero2){
        new ParametrosInvalidosException("O segundo parâmetro deve ser maior que o primeiro");
     }
}
